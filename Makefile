.PHONY: help prepare-dev gen

DATA_DIR=pandoc
PDF_ENGINE=lualatex
DATE_COVER=$(shell date "+%d %B %Y")

SOURCE_FORMAT=	markdown_strict$\
		+pipe_tables$\
		+backtick_code_blocks$\
		+auto_identifiers$\
		+strikeout$\
		+yaml_metadata_block$\
		+implicit_figures$\
		+all_symbols_escapable$\
		+link_attributes$\
		+smart$\
		+fenced_divs$\
		+tex_math_dollars
		
ARGS= -s\
      --dpi=300\
      --slide-level 2\
      --toc --listings\
      --shift-heading-level=0\
      --data-dir=${DATA_DIR}\
      --template default_mod.latex\
      --pdf-engine ${PDF_ENGINE}\
      -f ${SOURCE_FORMAT}\
      -M date='${DATE_COVER}'\
      -V classoption:aspectratio=169

.DEFAULT: help
help:
	@echo "make prepare environment"
	@echo "       prepare development environment"
	@echo "make gen"
	@echo "       generate pdf presentation"

prepare-dev:
	@echo "WIP"

gen:
	@pandoc ${ARGS} -t beamer content/1-introduction.md -o public/1-introduction.pdf 

clean:
	@rm public/*.pdf
