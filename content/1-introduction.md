---
title: "Drone 101 - Course"
subtitle: "Introduction"
author: "Jeferson Lima"
institute: "Federal Technological University of Paraná (UTFPR)"
topic: "Drone-101-Introduction"
theme: "Frankfurt"
colortheme: "beaver"
fonttheme: "professionalfonts"
mainfont: "Hack Nerd Font"
fontsize: 10pt
urlcolor: blue
linkstyle: bold
aspectratio: 169
date:
section-titles: true
toc: true
---

# Introduction

## Drone Types

![](content/figures/types-drone.png)

## Drone Types

::: columns
:::: {.column width=40%}
**Popular Drones**:

- [DJI Drone Series](https://www.dji.com/)

::::
:::: {.column width=60%}
![](content/figures/dji-examples.png)
::::
:::

## Drone Types

**Racing Drones**

::: columns
:::: {.column width=40%}

- Which is the best choice?
    - Buy a commercial racing drone.
    - or DIY!

::::
:::: {.column width=60%}
![](content/figures/diy-drone.jpg)
::::
:::

# Physics of a drone

## Motors Configuration

![](content/figures/types-model.png)


## Degree of Freedom 

::: columns
:::: {.column width=40%}
**Translation**

Translation ($X_I$, $Y_I$ and $Z_I$) means the displacement of a shape from one place to another.

::::
:::: {.column width=60%}
![](content/figures/euler_angles.png)
::::
:::


## Degree of Freedom 

::: columns
:::: {.column width=40%}


**Rotation**

A rotation (Roll($\phi$), Pitch($\theta$) and Yaw($\psi$)) is a transformation in which the object is rotated about a fixed point.

::::
:::: {.column width=60%}
![](content/figures/euler_angles.png)
::::
:::

## Degree of Freedom 

::: columns
:::: column
**Lift-off**
![](content/figures/degree-f1.png){height=70%}
::::
:::: column
**Roll Rotation**
![](content/figures/degree-f2.png){height=70%}
::::
:::
## Degree of Freedom 

::: columns
:::: {.column width=40%}
**Yaw Rotation**

The flight controller increases thrust on counterclockwise rotation motors and decreases thrust on clockwise rotation motors to rotate at yaw angle.

::::
:::: {.column width=60%}
![](content/figures/degree-f3.png){height=70%}
::::
:::

# FPV Drone Parts

::: columns
:::: {.column width=30%}

**Electrical Parts**

_a piece of cake!_

::::
:::: {.column width=70%}
![](content/figures/pinout.jpg)
::::
:::

## Flight Controller

::: columns
:::: {.column width=40%}

**Evolution of FC**

- CPU F(1,2,3,4 and 7) Series is STM32Fxx ARM Microcontrollers Series.

![](content/figures/stm32fc.jpg){height=90%}

::::
:::: {.column width=60%}
![](content/figures/fc-evolution.jpg){height=90%}
::::

:::
## Frame

## Radio Transmitter package

## Power Distribuition Board (PDB)

## Motors and Props

## Eletronic Speed Controller (ESC)

::: columns
:::: {.column width=40%}

**Evolution of FC**

- CPU F(1,2,3,4 and 7) Series is STM32Fxx ARM Microcontrollers Series.

![](content/figures/stm32fc.jpg){height=90%}

::::
:::: {.column width=60%}
![](content/figures/brushless_m.png){height=90%}
::::
:::

## Antennas

## batteries
**Lipo Battery Size**

_LIPO_ is Lithium polymer battery

|Num of Cells |Battery Voltage |Applications |
|---          |---             |---          |
|1S |3.7V |indor whoops |
|2S |7.4V | 30-75mm micro brushless drone|
|3S |11.1V | 100-220mm brushless drone|
|4S |14.8V | 220mm+ brushless drone|
|5S |18.5V | 220mm+ brushless drone|
|6S |22.2V | 220mm+ brushless drone|

## batteries

![](content/figures/battery-types.png){height=70%}

## batteries

**Quadcopter Battery C Rating**

The C rating of LiPo battery is a mensure of the safe and continuous maximum discharge current.

**Maximum Discarge Current = C-Rating x Capacity**

### Examples:

1. For 1200mAh - 50C battery has a estimated discharge current of 60A.
1. For 1300mAh - 120C battery has a estimated discharge is ___ A. 

# References

1. https://www.nasa.gov/sites/default/files/atoms/files/aam-science-behind-quadcopters-reader-student-guide_0.pdf
1. https://sanyambhutani.com/quadcopter-physics-explained/
1. https://oscarliang.com/flight-controller-explained/
